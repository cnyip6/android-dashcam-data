# Android dashcam data
Dashcam video clips and other data, captured using the camera and other sensors on an Android phone. Thought these might be useful for someone.

## Information
General information regarding the videos:

 
| **Video** | **Duration** | **Distance covered** |
| ----------- | ------------ | -------------------- |
| 1           | 6m23s        | 2.28km               |
| 2           | 7m44s        | 3.48km               |
| 3           | 4m17s        | 1.34km               |
| 4           | 2m8s         | 382m                 |
| 5           | 6m34s        | 2.52km               |
| **Total**   | 23m6s        | 10.002km             |

## Sensor data
Sensor data for the video are stored in a `.csv` file with the same name. The layout of the file is as follows:

| **Datum**     | **Description**                                                                                                                                                                                                                                                                                         | **Example**                     |
| ------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ------------------------------- |
| **Time**      | The time where the data entry is captured, accurate to the nearest milliseconds. The actual format of the timestamp string would be `yyyy-MM-dd'T'HH:mm:ss.SSSXXX` as specified in Java time formats.                                                                                                   | `2018-10-28T13:48:47.778+08:00` |
| **Heading**   | Geographical heading of the device, and thus the vehicle. The heading value is in absolute headings (azimuth), i.e. ranging from -180 to 180 degrees, starting from the magnetic north.                                                                                                                    | `46.83415985107422`             |
| **Pitch**     | Vertical position of the devices, and thus along the transverse axis of the vehicle. The values range from -180 degrees when the device tilt downwards, to 180 degrees when the device tilt upwards., as per the value specified in the Android sensor framework.                                       | `3.986057758331299`             |
| **Roll**      | Rotation position of the devices along the longitudinal axis of the vehicle, and thus the vehicle. The values range from -180 degrees when rotating to the left, to 180 degrees when the device rotate to the right, as per the value specified in the Android sensor framework.                        | `4.928718090057373`             |
| **Latitude**  | The latitude of the obtained location of the user. This value is directly pulled from the Android location manager using the onboard GPS locator as the source. The format used in the log file is in seconds, i.e `DDD:MM:SS.SSS`, where, `D` is the degree, `M` is the minute and `S` is the second.  | `22:16:39.6732`                 |
| **Longitude** | The longitude of the obtained location of the user. This value is directly pulled from the Android location manager using the onboard GPS locator as the source. The format used in the log file is in seconds, i.e `DDD:MM:SS.SSS`, where, `D` is the degree, `M` is the minute and `S` is the second. | `114:10:12.04536`               |
| **Speed**     | The speed of the user vehicle. Here speed is defined as earth speed, i.e the speed of the vehicle related to the surface of the Earth. This speed value is directly pulled from the Android location manager using the onboard GPS locator as the source, and is in format of meters per second.        | `3.390000104904175`             |

## License for use
All materials are licensed under the `CC BY 4.0` license.